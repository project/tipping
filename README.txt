
Tipping module

Description
-----------
With this module you can arrange a little game for your site users during 
the European championship. The aim is to predict the outcome of each match as close 
as possible and collect points for correct predictions.


Installation
------------
Drupal 5.x

   1. Copy the tipping folder to your modules directory.
   2. Enable the module under administer -> modules. The database tables 
      are created automatically.
   3. Change access permissions under administer -> permissions.
   4. Enable the 'Football World Cup' block under administer -> blocks.


Credits
-------
tenrapid (http://drupal.org/user/37587)
